#include <stdio.h>

void pattern(int x); //function to control the pattern
void Row(int y); //function to print each line of the pattern
int NoOfRows=1; //initial value for number of rows
int r; //variable to get the input

void pattern(int x)
{

    if(x>0)
    {
        Row(NoOfRows);
        printf("\n");
        NoOfRows++;
        pattern(x-1);

    }
}


void Row(int y)
{
    if(y>0)
    {
        printf("%d",y);
        Row(y-1);
    }
}

int main()
{

    printf("Enter the No. of Rows : ");
    scanf("%d",&r);
    pattern(r);
    return 0;
}

